#!/bin/bash -e
#
# Builds the afbeta docker image
#
# NOTE: Currently this requires that the user running this has
# permissions to access /var/run/docker.sock or equivilent on your
# platform.  If not you will either need to grant this user access to
# that resource or run this script as a higher power user and
# configure the aws cli for said higher power user.
#
. $(dirname $0)/af-shared-methods

function usage() {
    cat >&2 <<EOF
Usage: $(basename $0) <Project Name> [Project Root]
Builds a new Docker image and tags it under the given tag names:

* airfordable/<Project Name>:<Git Version>
* airfordable/<Project Name>:latest

Arguments:
  Project Name - The name of the project

EOF
}

# Args
PROJECT_NAME="$1"
PROJECT_ROOT="$2"

# Default Args.
[ -z "$PROJECT_ROOT" ] && PROJECT_ROOT="."


##
# Test our environment
##

# Check that required applications are here.
test_app_executable "docker"

# Test that we have the needed argument
test_defined 'PROJECT_NAME'

# Check that user can access docker daemon
check_docker

##
# If here then we are ready to rock and roll. Startup the build!
##

# Internal constants
BUILD_TAG_PFX="airfordable/${PROJECT_NAME}"
GIT_VERSION="$(af-git-version)"

# Get the project's name from the directory were the script is
# allocated
exec docker build \
     --build-arg AF_VERSION=$GIT_VERSION \
     -t "${BUILD_TAG_PFX}:${GIT_VERSION}" \
     "$PROJECT_ROOT"
